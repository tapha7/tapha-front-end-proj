import http from '../http-common';

class UserDataService {
  public getAll() {
    return http.get('user_profile');
  }

  public create(data: any) {
    return http.post('/user_profile', data);
  }

  public deleteAll() {
    return http.delete(`/user_profile`);
  }
}

export default new UserDataService();
