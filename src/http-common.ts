import axios from 'axios';

export default axios.create({
  baseURL: 'https://my-json-server.typicode.com/Tapha/TestDB/',
  headers: {
    'Content-type': 'application/json',
  },
});
