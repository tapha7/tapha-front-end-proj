import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueFormulate from '@braid/vue-formulate';
import './assets/tailwind.css' ;
import i18n from './i18n';

Vue.use(VueAxios, axios);
Vue.use(VueFormulate);

Vue.config.productionTip = false;

// Use beforeEach route guard to set the language
router.beforeEach((to, from, next) => {

  // Use the language from the routing param or default language
  let language: string = to.params.lang;
  if (!language || language !== 'de') {
    language = 'en';
  }

  // Set the current language for i18n.
  i18n.locale = language;
  next();
});

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
