# Egoditor Project (FrontEnd) - Tapha

# tapha-front-end-proj - description

This project recreates and implements the design of QRCodeGenerator as specified in the project spec. 

It does this in VueJS, TypeScript and TailwindCSS.

**Vue** as specified by the spec is a fantastic choice as it very easily lends itself to the kind of front end modularity, combined with performance, that is necessary for the modern web.

**TypeScript** for me is now a must-use for modern projects. It ensures proper typing as well as offers a suite of new features (such as decorators) that dramatically supercharge development productivity and quality.

**TailWindCSS** makes developing and implementing high quality, responsive CSS UI layouts incredibly fast and easy. It is highly intuitive and abstracts a lot of common CSS for you, so that you can focus on the more important elements of the UI implementation. 

Only necessary components have been separated out.

Tested for responsiveness, see screenshots here: [Link to screenshots](https://drive.google.com/file/d/1hhW_alPmAhj9-n13zkOVzL6scBxwJOHQ/view?usp=sharing)

**Language EN/DE**

You may switch between English and German by clicking on the two options on the top right of the navbar.

### Vue/Tailwind Implementation
![Vue/TailWind Implementation](tapha-front-end-proj.png)

### Before / After Screenshot (Original PNG vs Vue/Tailwind Implementation)
![Before/After](beforeafter.jpg)

# tapha-front-end-proj - installation

To install, simply clone this repository: `git clone https://tapha7@bitbucket.org/tapha7/tapha-front-end-proj.git` and then `cd` in to the project directory and run `npm install`.

## Relevant Components

### RegForm.vue

`/@components/RegForm.vue`
`/tests/unit/regform.spec.js`

This component loads in user data from a fake JSON API and also allows for the updating of that user data.

A simple test is included here for demonstration purposes (though it is a functional test and passes), and can be found in the directory specified above. It is implemented with Jest.

It can be run using: `npm run test:unit`.

[Watch it in action]()

### TopMainNav.vue

`/@components/TopMainNav.vue`

This component is the top nav display, separated out to have a cleaner UI.

## Commands to know

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
