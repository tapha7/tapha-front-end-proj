module.exports = {
  purge: [],
  theme: {
    colors: {
      blue: {
        default: '#00BFFF',
        dark: '#253F73',
        light: '#FDFEFF',
        link: '#0077DB'
      },
      black: '#111A35',
      gray: '#DE3618',
      white: {
        default: '#FFFFFF',
        darker: '#E4EDF0',
      },
      lightgrey: '#6B7E86',
      darklightgrey: '#F4F8FA',
    },
    extend: {},
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
  },
  plugins: [],
}