import { mount } from '@vue/test-utils';
import RegForm from '@/components/RegForm.vue';

// Test data within mounted RegForm to make sure it has pulled in user data.
describe('Mounted RegForm', () => {
    const wrapper = mount(RegForm);
    // Inspect that data pulled after mounting is there by checking status property for truthiness.
    it('has user data', () => {
        expect(wrapper.vm.$data.status).toBeTruthy;
    });
  },
);
